<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive.
 *
 * Override this template by copying it to yourtheme/woocommerce/archive-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

get_header( 'shop' ); ?>

	<?php
		/**
		 * woocommerce_before_main_content hook
		 *
		 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
		 * @hooked woocommerce_breadcrumb - 20
		 */
		do_action( 'woocommerce_before_main_content' );
	?>

	<div id="overlay" class="post-<?php the_ID(); ?>">
		<div id="over-cont">
			<h1>Halt</h1>
			<p>At ParrotDog it is required that any purchaser or acceptor of alcohol purchased through our online store confirms that they are aged 18 years or older.</p>
			<a href="http://parrotdog.co.nz">
				<div class="split">
					<p>I am under 18</p>
				</div>
			</a>
		
			<div id="eighteen" class="split secret">
				<p>I am 18 or over</p>
			</div>
		</div>
	</div>

		<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
			<div class="large-6 left">
				<h1 class="page-title"><?php woocommerce_page_title(); ?></h1>
			</div>
		<?php endif; ?>
		<div class="plz-buy">
			<a href="/cart/"><p>View Cart</p></a>
		</div>
		<div class="woocommerce-ordering">
			<select id="foo">
			    <option value="">Sort by Category</option>
			    <option value="/shop/">All</option>
			    <option value="/product-category/the-originals/">The Originals</option>
			    <option value="/product-category/four-packs/">Four Packs</option>
			    <option value="/product-category/limited-release/">Limited Release</option>
			    <option value="/product-category/clothing/">Clothing</option>
			    <option value="/product-category/posters/">Posters</option>
			    <option value="/product-category/glassware/">Glassware</option>
			    <option value="/product-category/vouchers/">Vouchers</option>
			</select>
		</div>
		
		

		<script>
		    document.getElementById("foo").onchange = function() {
		        if (this.selectedIndex!==0) {
		            window.location.href = this.value;
		        }        
		    };
		</script>

		<?php do_action( 'woocommerce_archive_description' ); ?>

		<?php if ( have_posts() ) : ?>

			<?php
				/**
				 * woocommerce_before_shop_loop hook
				 *
				 * @hooked woocommerce_result_count - 20
				 * @hooked woocommerce_catalog_ordering - 30
				 */
				do_action( 'woocommerce_before_shop_loop' );
			?>

			<?php woocommerce_product_loop_start(); ?>

				<?php woocommerce_product_subcategories(); ?>

				<?php while ( have_posts() ) : the_post(); ?>

				
					<div class="small-12 large-3 news">
						<?php wc_get_template_part( 'content', 'product' ); ?>
					</div>

				<?php endwhile; // end of the loop. ?>

			<?php woocommerce_product_loop_end(); ?>

			<?php
				/**
				 * woocommerce_after_shop_loop hook
				 *
				 * @hooked woocommerce_pagination - 10
				 */
				do_action( 'woocommerce_after_shop_loop' );
			?>

		<?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>

			<?php wc_get_template( 'loop/no-products-found.php' ); ?>

		<?php endif; ?>

	<?php
		/**
		 * woocommerce_after_main_content hook
		 *
		 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
		 */
		do_action( 'woocommerce_after_main_content' );
	?>

	

<?php get_footer( 'shop' ); ?>
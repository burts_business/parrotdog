<?php
/**
 * The Template for displaying all single products.
 *
 * Override this template by copying it to yourtheme/woocommerce/single-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

get_header( 'shop' ); ?>

	<?php
		/**
		 * woocommerce_before_main_content hook
		 *
		 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
		 * @hooked woocommerce_breadcrumb - 20
		 */
		do_action( 'woocommerce_before_main_content' );
	?>

	<div class="plz-buy">
			<a href="/cart/"><p>View Cart</p></a>
		</div>
		<div class="woocommerce-ordering">
			<select id="foo">
			    <option value="">Sort by Category</option>
			    <option value="/shop/">All</option>
			    <option value="/product-category/the-originals/">The Originals</option>
			    <option value="/product-category/four-packs/">Four Packs</option>
			    <option value="/product-category/limited-release/">Limited Release</option>
			    <option value="/product-category/clothing/">Clothing</option>
			    <option value="/product-category/posters/">Posters</option>
			    <option value="/product-category/glassware/">Glassware</option>
			    <option value="/product-category/vouchers/">Vouchers</option>
			</select>
		</div>

		<?php while ( have_posts() ) : the_post(); ?>

			<?php wc_get_template_part( 'content', 'single-product' ); ?>

		<?php endwhile; // end of the loop. ?>

	<?php
		/**
		 * woocommerce_after_main_content hook
		 *
		 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
		 */
		do_action( 'woocommerce_after_main_content' );
	?>

	
<?php get_footer( 'shop' ); ?>
<?php
/*
Template Name: More
*/
get_header(); ?>
	<div id="more" class="small-12 large-12" role="main">
		<div class="title-section homepage" style="background-image: url('http://parrotdog.burtsbusiness.com/wp-content/themes/ParrotDog-child/images/header-image.jpg');">
			<div class="intro-title">
				<h1>ABOUT US</h1>
			</div>
		</div>
	</div>

	<div id="about-section" class="clear" style="background: url('http://parrotdog.burtsbusiness.com/wp-content/themes/ParrotDog-child/images/beer-bkg.jpg'); 	background-position-y: bottom; background-repeat: repeat-x;">
		<div class="row introduction">
			<h4>ParrotDog is not just a bunch of mates</h4>
			<p>Started by three Matts, it definitely is a bunch of mates but ParrotDog has turned into so much more. Blah Blah more info here. A nice little intro perhaps.</p>
			<img width="250px;" src="<?php bloginfo('stylesheet_directory'); ?>/images/decal.svg" alt="decal"/>
		</div>
		
		
		<div class="row">	 
		   <a href="/our-story/">              
	           <div class="large-4 news">
				 <img src="http://parrotdog.burtsbusiness.com/wp-content/uploads/2014/09/article_01.jpg" alt="Our Story" />
				   <div class="about-container">
					   <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
							<h3>OUR STORY</h3>
							<p>From bath tubs to breweries, ParrotDog beer had a bit of an interesting start. Read more about our story here</p>
						   <a href="<?php the_permalink(); ?>"><p class="button">Read More</p></a>
					   </div>
				   </div>
			   </div>
		   </a>
			
		  	<a href="/the-brewery/">
				<div class="large-4 news">
					<img src="http://parrotdog.burtsbusiness.com/wp-content/themes/ParrotDog-child/images/brewery-article.jpg" alt="The Brewery" />
				   <div class="about-container">
					   <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						   <h3>THE BREWERY</h3>
						   <p>Take a look around our recently updated, space quality, international standard brewery based in Wellington, New Zealand</p>
						   <p class="button">Read More</p>
					   </div>
				   </div>
				</div>
			</a>
			
			<a href="/commercials/">
				<div class="large-4 news">
					<img src="http://parrotdog.burtsbusiness.com/wp-content/themes/ParrotDog-child/images/commercial-article.jpg" alt="Our Story" />
				   <div class="about-container">
					   <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
							<h3>THE COMMERCIALS</h3>
							<p>Our commercials don't even play on T.V. but they are still the talk of the town. Seriously. Ask anyone...</p>
						   <a href="<?php the_permalink(); ?>"><p class="button">Read More</p></a>
					   </div>
				   </div>
				 </div>
			 </a>

			<a href="/news/">
			 <div class="large-4 news">
				 <img src="http://parrotdog.burtsbusiness.com/wp-content/themes/ParrotDog-child/images/brewery-background-col.jpg" alt="Our Story" />
			   <div class="about-container">
				   <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<h3>THE EVENTS</h3>
					<p>A big part of ParrotDog is enjoying a few with friends old and new. We like to to this by throwing events. Find one near you	</p>
					<a href="<?php the_permalink(); ?>"><p class="button">Read More</p></a>
				   </div>
			   </div>
			 </div>
			</a>
			
			
			
		</div><!--End of the Row--->
	</div><!--End of About Section-->

	<!--<div class="">
		<div class="the-brewery more-than clearfix" style="background-image: url('http://parrotdog.burtsbusiness.com/wp-content/themes/ParrotDog-child/images/brewery-background-col.jpg');">
			<div class="heading">
				<h2><i>The</i> <span class="demi">BREWERY</span></h2>
			</div>
			<div class="info clearfix">
				<h1>THE<br/>BREWERY</h1>
				<a href="/the-brewery/"><div class="button">
					<p>TAKE A LOOK AROUND</p>
				</div><!-- button </a>
			</div><!-- info 
		</div><!-- the brewery 
		
		<div class="the-brewery commercials clearfix">
			<div class="heading">
				<h2><i>The</i> <span class="demi">COMMERCIALS</span></h2>
			</div>
			<div class="info clearfix">
				<h1>THE<br/>COMMERCIALS</h1>
				<a href="/commercials/"><div class="button">
					<p>WATCH THEM ALL</p>
				</div></a><!-- button 
			</div><!-- info
		</div><!-- the brewery -
		<div class="the-brewery more-than clearfix">
			<div class="heading">
				<h2><i>The</i> <span class="demi">EVENTS</span></h2>
			</div>
			<div class="info clearfix">
				<h1>EVENTS</h1>
				<a href="/category/events/"><div class="button">
					<p>WHATS GOING ON</p>
				</div><!-- button --</a>
			</div><!-- info --
		</div><!-- the brewery --

		<div class="the-brewery more-than clearfix">
			<div class="heading">
				<h2><i>The</i> <span class="demi">PROMOTIONS</span></h2>
			</div>
			<div class="info clearfix">
				<h1>PROMOTIONS</h1>
				<a href="/category/promotions/"><div class="button">
					<p>TAKE A LOOK AROUND</p>
				</div><!-- button --</a>
			</div><!-- info --
		</div><!-- the brewery --

	</div> 

</div>-->
<?php get_footer(); ?>

	</div>
</section>

<footer>
	<div class="row">
		<div class="large-2 small-6 left footer-col">
			<a href="/the-originals/"><h3>The Originals</h3></a>
			<ul>
				<?php $args = array( 'post_type' => 'beers', 'category_name' => 'core', 'posts_per_page' => 5, 'order' => 'ASC', 'orderby' => 'date'  );
				$loop = new WP_Query( $args );
				while ( $loop->have_posts() ) : $loop->the_post();?>
				  <li>
						<a href="<?php the_permalink();?>"><h4><?php the_title();?></h4></a>
				  </li>
				<?php endwhile;?>
			</ul>
		</div>
		<div class="large-2 small-6 left footer-col">
			<a href="/limited-release/"><h3 class="extra">Limited Releases</h3></a>
			<a href="/rarebird/"><h3 class="extra">RareBird</h3></a>
		</div>
		<div class="large-2 small-6 left footer-col">
			<a href="/shop/"><h3 class="extra">Shop Online</h3></a>
			<a href="/our-story/"><h3>About Us</h3></a>
			<ul>
				<a href="/our-story/"><li>Our Story</li></a>
				<a href="/the-brewery/"><li>The Brewery</li></a>
				<a href="/brewery-shop/"><li>Brewery Shop</li></a>
		
			</ul>
		</div>
		<div class="large-2 small-6 left footer-col">
			<a href="/news/"><h3>News</h3></a>
			<ul>
				<?php $args = array( 'category_name' => 'news', 'posts_per_page' => 3 );
				$loop = new WP_Query( $args );
				while ( $loop->have_posts() ) : $loop->the_post();?>
				  <li>
						<a href="<?php the_permalink();?>"><h4><?php the_title();?></h4></a>
				  </li>
				<?php endwhile;?>
			</ul>
			
			<!--<a href="/contact/"><h3>Contact Us</h3>
			<ul>
				<li><a href="mailto:info@parrotdog.co.nz">info@parrotdog.co.nz</a></li>
			</ul>-->
		</div>
		<div class="large-2 small-6 left footer-space">
			&nbsp;
		</div>
		<div class="large-2 small-6 left footer-col">
			<h3>Say Hello</h3>
			<ul>
				<li>ParrotDog</li>
				<li>29 Vivian Street</li>
				<li>Te Aro</li>
				<li>Wellington, 6011</li>
				<li>04 384 8077</li>
				<li><a href="mailto:info@parrotdog.co.nz">info@parrotdog.co.nz</a></li>
				
			</ul>
		</div>
	</div>
</footer>
<a class="exit-off-canvas"></a>

  </div>
</div>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-57815724-1', 'auto');
  ga('send', 'pageview');

</script>
<?php wp_footer(); ?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.pageslide.min.js"></script> 
<script src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.pageslide.js"></script> 
<script>
        /* Default pageslide, moves to the right */
        $(".first-this").pageslide();
        
        /* Slide to the left, and make it model (you'll have to call $.pageslide.close() to close) */
        $(".second-this").pageslide({ direction: 'left', modal: true });
    </script>
    
      <!-- jQuery -->
 
  <script>window.jQuery || document.write('<script src="js/libs/jquery-1.7.min.js">\x3C/script>')</script>

  <!-- FlexSlider -->
  <script defer src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.flexslider.js"></script>

  <script type="text/javascript">
    $(function(){
      SyntaxHighlighter.all();
    });
    $(window).load(function(){
      $('#main-slider').flexslider({
        animation: "slide",
        slideshow: false,
        start: function(slider){
          $('body').removeClass('loading');
        }
      });
    });
     $(window).load(function(){
      $('#secondary-slider').flexslider({
        animation: "slide",
        slideshowSpeed: 3000,
        start: function(slider){
          $('body').removeClass('loading');
        }
      });
    });
      $(window).load(function(){
      $('#news-slider').flexslider({
        animation: "slide",
        slideshowSpeed: 3000,
        slideshow: false,
        start: function(slider){
          $('body').removeClass('loading');
        }
      });
    });
     $(window).load(function(){
      $('.flexslider').flexslider({
        animation: "slide",
        slideshowSpeed: 3000,
        start: function(slider){
          $('body').removeClass('loading');
        }
      });
      
        $('#beers-slider').flexslider({
	    animation: "slide",
	    controlNav: false,
	    itemWidth: 500,
	    animationLoop: false,
	    slideshow: false,
	    sync: "#carousel"
	  });

    });
  </script>
  
  <script>
  $(document).ready(function() {
	   var container_width = SINGLE_IMAGE_WIDTH * $(".container-inner a").length;
	   $(".container-inner").css("width", container_width);
	});
</script>
<script src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.overscroll.js"></script>
<script>
	$(".container-outer").overscroll({
	direction: 'horizontal'
});
</script>


</body>
</html>
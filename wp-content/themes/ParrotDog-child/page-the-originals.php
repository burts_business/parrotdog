<?php get_header(); ?>
<div id="beer-page" class="small-12 large-12" role="main">
	<div class="title-section homepage">
			<div class="intro-title">
				<h1><?php the_title(); ?></h1>
			</div>
		</div>

	
	<div id="beer-section" class="clear" style="background: url('http://parrotdog.burtsbusiness.com/wp-content/themes/ParrotDog-child/images/beer-bkg.jpg'); 	background-position-y: bottom; background-repeat: repeat-x; padding-bottom:50px; margin-top: -20px;">
		<div class="row introduction">
			<h4>These are the originals.</h4>
			<p>Brewed year round, always enjoyable, always appropriate.<br> True to what we do - just real nice.</p>
			<img width="250px;" src="<?php bloginfo('stylesheet_directory'); ?>/images/decal.svg" alt="decal"/>
		</div>
	
		
		
		<div class="container-outer">
		   <div class="container-inner">
		   
		 <?php $args = array( 'post_type' => 'beers', 'category_name' => 'core', 'posts_per_page' => -1, 'order' => 'ASC' );
		$loop = new WP_Query( $args );?>
		
		<?php while ( $loop->have_posts() ) : $loop->the_post();?>
				<a href="<?php the_permalink(); ?>">
					<div class="mug-shot tap-badges">
					<?php if ( has_post_thumbnail() ) { the_post_thumbnail(); } ?>
					
						<a href="<?php the_permalink();?>">
							<div class="beer-home home-hover colour-<?php the_ID(); ?>">
								<h3><?php the_field('homepage_name');?></h3>
							
								<div class="bottom-feeder">
									<p><span class="demi"><?php the_field('type');?> - <?php the_field('type/percentage'); ?></span></p>
									<hr>
									<p>Find out more</p>	
								</div>		
							</div>
						</a>	
					</div>
				</a>
				<?php endwhile; wp_reset_query(); ?>
				
		   </div>
		</div>
		
		<div class="row introduction">
			<p>Click and Drag to Navigate</p>
			<img width="250px;" src="<?php bloginfo('stylesheet_directory'); ?>/images/decal.svg" alt="decal"/>
		</div>

		
	</div><!-- beer section -->			
</div>	
<?php get_footer(); ?>

<?php get_header(); ?>
<div id="beer-page" class="small-12 large-12" role="main">
	<div class="title-section homepage">
			<div class="intro-title">
				<h1><?php the_title(); ?></h1>
			</div>
		</div>

	
	<div id="beer-section" class="clear" style="background: url('http://parrotdog.burtsbusiness.com/wp-content/themes/ParrotDog-child/images/beer-bkg.jpg'); 	background-position-y: bottom; background-repeat: repeat-x; padding-bottom:50px; margin-top: -20px;">
		<div class="row introduction">
			<h4>Something a little bit special.</h4>
			<p>These ones are seasonal, born from collaborative projects and events; that kind of thing. Because they are elusive, and with a slightly more distinct look than the originals, there’s a fair bit of nonchalance and character going on. It’s worth checking out what interesting brews are available here:</p>
			<img width="250px;" src="<?php bloginfo('stylesheet_directory'); ?>/images/decal.svg" alt="decal"/>
		</div>
		
		<div class="container-outer">
		   
		   <div class="container-inner">
			<!--<div id="arrowL">
		    </div>
		    <div id="arrowR">
		    </div>-->

		   
		 <?php $args = array( 'post_type' => 'beers', 'category_name' => 'specials', 'posts_per_page' => -1, 'order' => 'ASC' );
		$loop = new WP_Query( $args );?>
		
		<?php while ( $loop->have_posts() ) : $loop->the_post();?>
		<a href="<?php the_permalink(); ?>">
		
				<div class="mug-shot tap-badges">
				<?php if ( has_post_thumbnail() ) { the_post_thumbnail(); } ?>
				
				<div class="beer-home item home-hover colour-<?php the_ID(); ?>">
					<a href="<?php the_permalink();?>">
						<h3><?php the_field('homepage_name');?></h3>
					
						<div class="bottom-feeder">
							<p><span class="demi"><?php the_field('type');?> - <?php the_field('type/percentage'); ?></span></p>
							<hr>
							<p>Find out more</p>	
						</div>
					</a>			
				</div>
				</div>
				<?php endwhile; wp_reset_query(); ?>
			
				
		     </a>
		   </div>
		</div>
		
		<div class="row introduction">
			<p>Click and Drag to Navigate</p>
			<img width="250px;" src="<?php bloginfo('stylesheet_directory'); ?>/images/decal.svg" alt="decal"/>
		</div>
		
	</div><!-- beer section -->			
</div>	
<?php get_footer(); ?>

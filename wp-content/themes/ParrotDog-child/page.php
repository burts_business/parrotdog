<?php get_header(); ?>

	<div id="woocommerce" class="small-12 large-12" role="main">
		<div class="title-section homepage" style="background-image: url('http://parrotdog.burtsbusiness.com/wp-content/themes/ParrotDog-child/images/header-image.jpg');">
			<div class="intro-title">
				<h1><?php the_title(); ?></h1>
			</div>
		</div>
	</div>

	<div id="about-section" class="clear" style="background: url('http://parrotdog.burtsbusiness.com/wp-content/themes/ParrotDog-child/images/beer-bkg.jpg'); 	background-position-y: bottom; background-repeat: repeat-x;">
		
			
	
			<div class="row">
				
				
				 
               
			     <?php if (have_posts()) : ?>
			               <?php while (have_posts()) : the_post(); ?>    
			             
						   	<?php the_content(); ?>
			               <?php endwhile; ?>
			     <?php endif; ?>
			
			
			
	</div><!--End of About Section-->

</div>
<?php get_footer(); ?>
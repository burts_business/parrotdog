<?php get_header(); ?>
	<div class="small-12 large-12" role="main">
		<div class="title-section homepage" style="background-image: url('http://parrotdog.burtsbusiness.com/wp-content/themes/ParrotDog-child/images/header-image.jpg');">
			<div class="intro-title">
				<h1>Promotions</h1>
			</div>
		</div>
		
	
		
		<div class="dark clearfix">
			<div class="heading black">
				<h2><i>The</i> <span class="demi">PROMOTIONS</span></h2>
			</div>
			<div class="row">
			
			  	 <?php $args = array( 'posts_per_page' => -1, 'category_name' => 'promotions', 'order' => 'ASC' );
					$loop = new WP_Query( $args );?>
					<?php while ( $loop->have_posts() ) : $loop->the_post();?>
   
			               
			               <div class="large-4 left news">
								   <a href="<?php the_permalink(); ?>"><?php if ( has_post_thumbnail() ) {
								   the_post_thumbnail();
								   } ?></a>
								   <div class="news-container">
								   <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
									   <h3><?php the_title(); ?></h3>
									   <p><?php $excerpt = get_the_excerpt();
											  echo string_limit_words($excerpt,20);?>
										</p>
									   <a href="<?php the_permalink(); ?>"><p class="button">Read More</p></a>
								   </div>
							   </div>
			               </div>

			               
				<?php endwhile; wp_reset_query(); ?>
			
			</div>
		</div>
		
			
		
	
	<!--<?php while (have_posts()) : the_post(); ?>
		<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
			<header>
				<h1 class="entry-title"><?php the_title(); ?></h1>
			</header>
			<div class="entry-content">
				<?php the_content(); ?>
			</div>
			<footer>
				<?php wp_link_pages(array('before' => '<nav id="page-nav"><p>' . __('Pages:', 'FoundationPress'), 'after' => '</p></nav>' )); ?>
				<p><?php the_tags(); ?></p>
			</footer>
		</article>
	<?php endwhile;?>-->

	</div>
		
<?php get_footer(); ?>
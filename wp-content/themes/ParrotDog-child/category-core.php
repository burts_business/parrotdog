<?php get_header(); ?>
<div id="beer-page" class="small-12 large-12" role="main">
	<div class="title-section homepage">
			<div class="intro-title">
				<h1><?php the_title(); ?></h1>
			</div>
		</div>

	
	<div id="beer-section" class="clear" style="background: url('http://parrotdog.burtsbusiness.com/wp-content/themes/ParrotDog-child/images/beer-bkg.jpg'); 	background-position-y: bottom; background-repeat: repeat-x; padding-bottom:50px; margin-top: -20px;">
		<div class="row introduction">
			<h4>ParrotDog Beer is Really Nice.</h4>
			<p>Donec sed odio dui. Maecenas faucibus mollis interdum. Donec ullamcorper nulla non metus auctor fringilla.</p>
			<img width="250px;" src="<?php bloginfo('stylesheet_directory'); ?>/images/decal.svg" alt="decal"/>
		</div>
	
		
		<div class="container-outer">
		   <div class="container-inner">
		   
		 <?php $args = array( 'post_type' => 'beers', 'category_name' => 'core', 'posts_per_page' => -1, 'order' => 'ASC' );
		$loop = new WP_Query( $args );?>
		
		<?php while ( $loop->have_posts() ) : $loop->the_post();?>

				<div class="mug-shot tap-badges">
				<?php if ( has_post_thumbnail() ) { the_post_thumbnail(); } ?>
				
				<div class="beer-home home-hover colour-<?php the_ID(); ?>">
					<a href="<?php the_permalink();?>">
						<h3><?php the_field('homepage_name');?></h3>
					
						<div class="bottom-feeder">
							<p><span class="demi"><?php the_field('type/percentage'); ?></span></p>
							<hr>
							<p>Find out more</p>	
						</div>
					</a>			
				</div>
				</div>
				<?php endwhile; wp_reset_query(); ?>
			
				
		     </a>
		   </div>
		</div>
		
		
	</div><!-- beer section -->			
</div>	
<?php get_footer(); ?>

<?php get_header(); ?>

<div class="beer-hero clearfix background-image-<?php the_ID(); ?>">
	<div class="beer-info clearfix">
		<div class="left <?php the_field('text-col'); ?>">
			<h1 class="background-colour-<?php the_ID(); ?>"><?php the_field('homepage_name');?></h1>
			<p class="abv"><?php the_field('type/percentage'); ?></p>
			<p class="type background-colour-<?php the_ID(); ?>"><?php the_field('type');?></p>
			     <?php if (have_posts()) : ?>
			               <?php while (have_posts()) : the_post(); ?>    
			              
			<div class="excerpt"><?php the_content(); ?></div>
			          
		</div><!-- left -->
		<div class="right clearfix">
			<?php 

				$image = get_field('main_image');

				if( !empty($image) ): ?>

					<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

			<?php endif; ?>
			
		</div><!-- right -->
	</div><!-- beer info -->

	<div class="next-beer">
		<p><span><?php next_post('%', '', 'yes'); ?></span></p>
	</div>
	<div class="previous-beer">
		<p><span><?php previous_post('%', '', 'yes'); ?></span></p>
	</div><!-- previous beer -->
</div><!-- hero -->
 <?php endwhile; ?>
			  <?php endif; ?>
<div class="divider"></div>

<div class="order-online clearfix">
	<div class="left clearfix">
		<p>Availability:</p>
		<div class="icons clearfix">
			<div class="single <?php if(get_field('single') == "Not Available"){echo "not-available";};?>">
				<img src="<?php bloginfo('stylesheet_directory'); ?>/images/beer.svg" alt="Single">
				<p>Singles</p>
			</div>
			<div class="packs <?php if(get_field('packs') == "Not Available"){echo "not-available";};?>">
				<img src="<?php bloginfo('stylesheet_directory'); ?>/images/box.svg" alt="Pack">
				<p>Packs</p>
			</div>
			<!--<div class="tap <?php if(get_field('on_tap') == "Not Available"){echo "not-available";};?>">
				<img src="<?php bloginfo('stylesheet_directory'); ?>/images/tap.svg" alt="Tap">
				<p>On Tap</p>
			</div>-->
			<div class="keg <?php if(get_field('keg') == "Not Available"){echo "not-available";};?>">
				<img src="<?php bloginfo('stylesheet_directory'); ?>/images/keg.svg" alt="Keg">
				<p>Kegs</p>
			</div>
		</div><!-- icons -->
		<a href="<?php the_field('order_link');?>">
			<div class="button hover-background-colour-<?php the_ID(); ?>">
				<p class="background-colour-<?php the_ID(); ?>">ORDER ONLINE</p>
			</div><!-- button -->
		</a>
	</div><!-- left -->
	<div class="right clearfix">
		<div class="quote">
			<h1 class="background-colour-<?php the_ID(); ?>"><?php the_field('quote');?></h1>
			<p class="person"><?php the_field('quote_by');?></p>
		</div><!-- quote -->
	</div><!-- right -->
</div><!-- order online -->

<!--<div class="instafeed-container clearfix">
	<?php $image = get_field('pack_shots');

			if( !empty($image) ): ?>

			<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

			<?php endif; ?>
			<?php $image = get_field('pack_shots');

			if( !empty($image) ): ?>

			<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

			<?php endif; ?>
			<?php $image = get_field('pack_shots');

			if( !empty($image) ): ?>

			<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

			<?php endif; ?>
			<?php $image = get_field('pack_shots');

			if( !empty($image) ): ?>

			<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

			<?php endif; ?>
			<?php $image = get_field('pack_shots');

			if( !empty($image) ): ?>

			<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

			<?php endif; ?>
			<?php $image = get_field('pack_shots');

			if( !empty($image) ): ?>

			<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

			<?php endif; ?>
		
			
	
	<div class="overlay">
		<h1>#<?php the_title(); ?><br>@PARROTDOGBEER</h1>
	</div><!-- overlay -->
</div><!-- instafeed container -->


<div class="the-brewery clearfix">
	<div class="heading">
		<h2><i>The</i> <span class="demi">BREWERY</span></h2>
	</div>
	<div class="info clearfix">
		<h1>THE<br/>BREWERY</h1>
		<a href="/the-brewery/">
			<div class="button">
				<p>TAKE A LOOK AROUND</p>
			</div><!-- button -->
		</a>
	</div><!-- info -->
</div>


<?php get_footer(); ?>
<?php get_header(); ?>
	<div id="news-single" class="small-12 large-12" role="main">
	<?php
		$post_image_id = get_post_thumbnail_id($post_to_use->ID);
		if ($post_image_id) {
			$thumbnail = wp_get_attachment_image_src( $post_image_id, 'post-thumbnail', false);
			if ($thumbnail) (string)$thumbnail = $thumbnail[0];
		}
		?>
		<div class="title-section homepage" style="background-image: url('http://parrotdog.burtsbusiness.com/wp-content/themes/ParrotDog-child/images/header-image-news.jpg');">

			<div class="intro-title">
				<a href="/news/"><h1>NEWS</h1></a>
				<h2><?php the_title(); ?></h2>
			</div>
		</div>
		
		
		
		<div class="com-tup">
			<div class="row clearfix tupperware">
				<div class="large-6 left">
					     <?php if (have_posts()) : ?>
					               <?php while (have_posts()) : the_post(); ?>  
					               <h4><?php the_title(); ?></h4> 
					               <h5><?php the_date(); ?></h5>
						               <?php the_content() ?>
									   
					               <?php endwhile; ?>
					     <?php endif; ?>
					
				</div>
				<div class="large-6 left module-right">
					
					<?php if( have_rows('gallery') ):?>
				
					<?php while ( have_rows('gallery') ) : the_row();?>
				
			       		<?php $image = get_sub_field('images');
			        	if( !empty($image) ): ?>		
							<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
						<?php endif; ?>
				
				<?php endwhile;
				else :
				{
					the_post_thumbnail();
					} 
					
				endif;?>	  	
					<p></p>
					<?php next_post('&laquo; %', '', 'yes'); ?> | <?php previous_post('% &raquo; ', '', 'yes'); ?>
			
					
				</div>
			</div>
			
		
	</div>
		
<?php get_footer(); ?>
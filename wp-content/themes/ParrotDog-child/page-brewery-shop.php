<?php get_header(); ?>
<div class="small-12 large-12" role="main">
	<div class="title-section homepage" style="background-image: url('http://parrotdog.burtsbusiness.com/wp-content/themes/ParrotDog-child/images/brewery-background-3.jpg');">
		
			<div class="intro-title">
				<h1><?php the_title(); ?></h1>
			</div>
		</div>
</div>

<div class="row clearfix tupperware">
		<div class="large-6 small-12 left">
			 <?php if (have_posts()) : ?>
	               <?php while (have_posts()) : the_post(); ?>    
	         
				   		<p><?php the_content(); ?></p>
	   
	               <?php endwhile; ?>
	     <?php endif; ?>
		</div>
		<div class="large-6 small-12" style="float:left;">
			<img src="<?php bloginfo('stylesheet_directory'); ?>/images/cellar.jpg" alt="Cellar Door" />
		</div>
</div>


<!--<div class="center eighty eighty-text">

	<h4<?php the_title(); ?></h4>
	<?php if (have_posts()) : ?>
       <?php while (have_posts()) : the_post(); ?>    
 
	   		<p><?php the_content(); ?></p>

       <?php endwhile; ?>
     <?php endif; ?>
	
</div>

<img class="eighty" src="<?php bloginfo('stylesheet_directory'); ?>/images/cellar.jpg" alt="Cellar Door" />
-->

<div class="row introduction">


	<h4>What's on tap?</h4>
	<p>Each month we tag in a few Limited Releases to work in with The Originals. Here's what we have on offer at the moment:</p>
</div>

<div class="on-tap clearfix">
<?php
// check if the repeater field has rows of data
if( have_rows('tap_selection') ):
 	// loop through the rows of data
    while ( have_rows('tap_selection') ) : the_row();?>

       <?php $post_object = get_sub_field('page_link');
			if($post_object) :
			    $post = $post_object;?>
			    
			    <?php setup_postdata( $post ); ?>
			    
		<a href="<?php the_permalink(); ?>">	    
			<div class="beer-circle">
				<img src="<?php bloginfo('stylesheet_directory'); ?>/images/tap.svg" alt="Tap">
				<p><?php the_title(); ?></p>
			</div>
		</a>
		
		   <?php wp_reset_postdata(); ?>
		   <?php endif; ?>

    <?php endwhile;
else :
    // no rows found

endif;
?>


	
	<!--<div class="beer-circle">
		<img src="<?php bloginfo('stylesheet_directory'); ?>/images/tap.svg" alt="Tap">
		<p>FlaxenFeather</p>
	</div>
	<div class="beer-circle">
		<img src="<?php bloginfo('stylesheet_directory'); ?>/images/tap.svg" alt="Tap">
		<p>PitBull</p>
	</div>
	<div class="beer-circle">
		<img src="<?php bloginfo('stylesheet_directory'); ?>/images/tap.svg" alt="Tap">
		<p>DevilBird</p>
	</div>
	<div class="beer-circle">
		<img src="<?php bloginfo('stylesheet_directory'); ?>/images/tap.svg" alt="Tap">
		<p>Dogg</p>
	</div>
	<div class="beer-circle">
		<img src="<?php bloginfo('stylesheet_directory'); ?>/images/tap.svg" alt="Tap">
		<p>Otis</p>
	</div>-->
</div>

<div class="row introduction">
	<img width="250px;" src="<?php bloginfo('stylesheet_directory'); ?>/images/decal.svg" alt="decal"/>
</div>

<div class="row center">
	<div class="thirty small-12">
		<h2>Opening Hours</h2>
		<p>Monday - Thurs: 10am – 6pm<br>
		Friday: 10am – 8pm<br>
		Saturday: – 12pm -6pm</p>
	</div>
	<div class="thirty small-12">
		<h2>Address</h2>
		<p>29 Vivian Street,<br>
		 Te Aro,<br> 
		 Wellington, 6011</p>
	</div>
	<div class="thirty small-12">
		<h2>Contact</h2>
		<p>Phone: 04 384 8077<br>
		<a href="mailto:info@parrotdog.co.nz">Email: info@parrotdog.co.nz</a></p>
	</div>
</div>

<div class="row images">
	<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d2997.557217273631!2d174.78021999999999!3d-41.29674019999999!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6d38afdbbdd214c5%3A0x4236030848e681e0!2s601%2F29+Vivian+St%2C+Te+Aro%2C+Wellington+6011!5e0!3m2!1sen!2snz!4v1417489833862" width="1000" height="450" frameborder="0" style="border:0"></iframe>
</div>

<!--<div class="row images">
	<div class="small-12 large-6 left">
		<img src="<?php bloginfo('stylesheet_directory'); ?>/images/brewery-4.jpg" alt="brewery"/>
	</div>
	<div class="small-12 large-6 left">
		<img src="<?php bloginfo('stylesheet_directory'); ?>/images/brewery-6.jpg" alt="brewery"/>
	</div>
</div>-->


<?php get_footer(); ?>

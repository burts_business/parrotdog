<?php
/*
Template Name: Home
*/
get_header('home'); ?>
<div id="home" class="beer-hero clearfix">
	
				
		<div id="beer-section" class="clear">
			
			<div>
			<div class="row">
				<h1>REAL NICE.</h1>
				<div>
			<?php $args = array( 'post_type' => 'beers', 'category_name' => 'core', 'posts_per_page' => 4, 'order' => 'ASC', 'orderby' => 'date'  );
				$loop = new WP_Query( $args );?>
				
				<?php while ( $loop->have_posts() ) : $loop->the_post();?>

							<div class="mug-shot large-3">
							
							
							<?php 
	
								$image = get_field('main_image');
	
								if( !empty($image) ): ?>
	
								<a href="<?php the_permalink(); ?>"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></a>
	
							<?php endif; ?>
	
							
							<div class="beer-home home-hover colour-<?php the_ID(); ?>">
								<a href="<?php the_permalink();?>">
									<h3><?php the_field('homepage_name');?></h3>
								
									<div class="bottom-feeder">
										<p><span class="demi"><?php the_field('type');?> - <?php the_field('type/percentage'); ?></span></p>
										<hr>
										<p>Find out more</p>	
									</div>
								</a>			
							</div>
							</div>
							<?php endwhile; wp_reset_query(); ?>
						</div>
					</div>
				
			</div>	
			</div>
		</div>
		
		<div class="row clearfix tupperware video">
		  <div id="main-slider" class="flexslider">
	          <ul class="slides">
		          <li>
					<div class="large-8 small-12 left">
						<style>.embed-container { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto; } .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }</style><div class='embed-container'><iframe src='https://player.vimeo.com/video/113980224?title=0&amp;byline=0&amp;portrait=0' frameborder='0' webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div>
					</div>
					<div class="large-4 small-12 left module-right">
						<h2>New Human</h2>
						<p><span class="demi">Oop, there he is, hiding in the brushes, searching for berries. Look at him. He's brand new.</span></p>
						<p>Check out our nice new advert created by the very talented Sam Krispstifski over at Curious Films.</p>
						<!--<a href="/commercials/" class="button">See more videos</a>-->
					</div>
		          </li>
		          <li>
					<div class="large-8 small-12 left">
						<style>.embed-container { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto; } .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }</style><div class='embed-container'><iframe src='https://player.vimeo.com/video/113323249?title=0&amp;byline=0&amp;portrait=0' frameborder='0' webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div>
						
						
					</div>
					<div class="large-4 small-12 left module-right">
						<h2>Slide</h2>
						<p><span class="demi">Hey Sal, why don't you show us that thing. Yea Sal, show us that thing. ParrotDog Beer. Nice.</p>
						<p>The original ParrotDog Beer ad. Congrats.</p>
						<!--<a href="/commercials/" class="button">See more videos</a>-->
					</div>
		          </li>
	          </ul>
		  </div>
		</div>
		
		<div class="latest-news clearfix news dark">
			<div class="heading">
				<a href="/news/"><h2><i>Interesting</i> <span class="demi">STUFF</span></h2></a>
			</div>
			<div class="tupperware row">
					<div id="news-slider" class="flexslider">
						<ul class="slides">
						<?php $args = array('category_name' => 'news', 'posts_per_page' => -1, 'order' => 'DESC' );
						$loop = new WP_Query( $args );?>		
						<?php while ( $loop->have_posts() ) : $loop->the_post();?>
							<li>
								<div class="large-8 left">
									<?php if ( has_post_thumbnail() ) {
									   the_post_thumbnail();
									   } ?>
								</div>
								<div class="large-4 news">
									<div class="news-container">
									   <div <?php post_class(); ?>>
										   <h3><?php the_title(); ?></h3>
										   <p><?php $excerpt = get_the_excerpt();
												  echo string_limit_words($excerpt,20);?>
											</p>
										  <a href="<?php the_permalink(); ?>"><p class="button">Read More</p></a>
									   </div>
									</div>
								</div>
							</li>
			               
			              <?php endwhile; wp_reset_query(); ?>
						</ul>
					</div>
			</div>
		</div>
		
		<div class="clearfix">
			<div class="heading">
				<h2><i>Limited</i> <a href="/limited-release/"><span class="demi">RELEASES</span></a></h2>
			</div>
			<div class="specials">
				 			
			</div>
     	</div>
	 	
	 	<div id="limited-slide">
	        <div class="flexslider">
	          <ul class="slides">
	          <?php $args = array( 'post_type' => 'beers', 'category_name' => 'specials', 'posts_per_page' => -1, 'order' => 'ASC' );
					$loop = new WP_Query( $args );?>		
					<?php while ( $loop->have_posts() ) : $loop->the_post();?>
					<li>			
						<?php
						$image = get_field('special_banner');
		
						if( !empty($image) ): ?>
		
							<a href="<?php the_permalink(); ?>"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></a>
		
						<?php endif; ?>
							<!--<div class="beer-home home-hover colour-<?php the_ID(); ?>">
								<a href="<?php the_permalink();?>">
									<h3><?php the_field('homepage_name');?></h3>
								
									<div class="bottom-feeder">
										<p><span class="demi"><?php the_field('type');?> - <?php the_field('type/percentage'); ?></span></p>
										<hr>
										<p>Find out more</p>	
									</div>
								</a>			
							</div>-->

					</li>			
					<?php endwhile; wp_reset_query(); ?>	          	    		
	          </ul>
	        </div>
	 	</div>

	</div>
		
<?php get_footer(); ?>
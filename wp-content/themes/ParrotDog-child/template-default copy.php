<?php
/*
Template Name: Default
*/
get_header(); ?>
<div class="small-12 large-12" role="main">
	<div class="title-section homepage">
			<div class="intro-title">
				<h1><?php the_title(); ?></h1>
			</div>
		</div>
</div>
<div class="order-online clearfix">
	<div class="sixty clearfix">
		<p>Availability:</p>
		<div class="icons clearfix">
			<div class="single <?php if(get_field('single') == "Not Available"){echo "not-available";};?>">
				<img src="<?php bloginfo('stylesheet_directory'); ?>/images/beer.svg" alt="Single">
				<p>Singles</p>
			</div>
			<div class="packs <?php if(get_field('packs') == "Not Available"){echo "not-available";};?>">
				<img src="<?php bloginfo('stylesheet_directory'); ?>/images/box.svg" alt="Pack">
				<p>Packs</p>
			</div>
			<div class="tap <?php if(get_field('on_tap') == "Not Available"){echo "not-available";};?>">
				<img src="<?php bloginfo('stylesheet_directory'); ?>/images/tap.svg" alt="Tap">
				<p>On Tap</p>
			</div>
			<div class="keg <?php if(get_field('keg') == "Not Available"){echo "not-available";};?>">
				<img src="<?php bloginfo('stylesheet_directory'); ?>/images/keg.svg" alt="Keg">
				<p>Kegs</p>
			</div>
		</div><!-- icons -->
	</div><!-- left -->
	<div class="forty clearfix">
		<div class="quote">
			<h2 class="background-colour-<?php the_ID(); ?>">Opening Hours</h2>
			<p class="person">Monday to Friday: 9am -6pm<br>
			Saturday: 11am -8pm</p>
		</div><!-- quote -->
	</div><!-- right -->
	<div class="clear"></div>
	<div class="sixty">
		<p>Nullam quis risus eget urna mollis ornare vel eu leo. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Maecenas sed diam eget risus varius blandit sit amet non magna. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
	</div>
	<div class="forty">
		<h2 class="background-colour-<?php the_ID(); ?>">Address</h2>
		<p>13 National Street</p>
		<p>Winstonville</p>
		<p>Wellington</p>
	</div>
</div><!-- order online -->

	
<?php get_footer(); ?>

<?php get_header(); ?>
<div id="about" class="small-12 large-12" role="main">
	<div class="title-section homepage">
		<div class="intro-title">
			<h1>OUR STORY</h1>
		</div>
	</div>
	<div class="row introduction">
		<h4><?php the_field('intro_title'); ?></h4>
		<p><?php the_field('intro_content'); ?></p>
		<img width="250px;" src="<?php bloginfo('stylesheet_directory'); ?>/images/decal.svg" alt="decal"/>
	</div>
	<div class="row clearfix tupperware">
		<div class="large-6 left">
			 <?php if (have_posts()) : ?>
	               <?php while (have_posts()) : the_post(); ?>    
	         
				   		<p><?php the_content(); ?></p>
	   
	               <?php endwhile; ?>
	     <?php endif; ?>
		</div>
		<div class="large-6" style="float:left;">
			<div class="flexslider">
			<?php if( have_rows('gallery') ):?>
				<ul class="slides">	
				<?php while ( have_rows('gallery') ) : the_row();?>
					<li>
			       		<?php $image = get_sub_field('images');
			        	if( !empty($image) ): ?>		
							<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
						<?php endif; ?>
					</li>
				<?php endwhile;
				else :?>
				</ul>
				<?php endif;?>	  		  
			</div>
			<img style="width:75%; margin:12.5%;" src="<?php bloginfo('stylesheet_directory'); ?>/images/first-poster.jpg" alt="Parrot Dog"/>
		</div>
		
	</div>

	<div class="the-brewery clearfix">
		<div class="heading">
			<h2><i>The</i> <span class="demi">BREWERY</span></h2>
		</div>
		<div class="info clearfix">
			<h1>THE<br/>BREWERY</h1>
			<a href="/the-brewery/">
				<div class="button">
					<p>TAKE A LOOK AROUND</p>
				</div><!-- button -->
			</a>
		</div><!-- info -->
	</div>
</div>
<?php get_footer(); ?>


<?php get_header(); ?>
<div class="small-12 large-12 columns" role="main">
	<div class="intro-section">
	<h1>INTRO SLIDESHOW</h1>
	</div>
	
	<div class="row content">
		<div class="page-title large-8">
			<h1>Complete Range</h1>
		</div>
		<div class="range">
			<div class="title"><h2>Core Range</h2></div>
			<div class="emblems">
			
			<?php query_posts('post_type=beers&category_name=core'); ?>
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					<a href="<?php the_permalink(); ?>"><h3><?php the_title(); ?></h3></a>
					<?php the_content(); ?>
			<?php endwhile; else: ?>
				<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
			<?php endif; ?>
			
			</div>
							
		</div>
		<div class="range">
			<div class="title"><h2>Seasonal Range</h2></div>
			<div class="emblems">
			
			<?php query_posts('post_type=beers&category_name=seasonal'); ?>
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					<a href="<?php the_permalink(); ?>"><h3><?php the_title(); ?></h3></a>
					<?php the_content(); ?>
			<?php endwhile; else: ?>
				<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
			<?php endif; ?>

				
			
			</div>

		</div>
	</div>
</div>

SOMETHING
<?php get_footer(); ?>

<?php get_header(); ?>

<div class="small-12 large-12 columns" role="main">
	<div class="intro-section">
	<h1>INTRO SLIDESHOW</h1>
	</div>
	
	<div class="content">
		<div class="row">
			<div class="large-8 left">
				<div class="page-title">
					<h1><?php the_title(); ?></h1>
				</div>
				<div class="beer-cat">	
					<p><?php the_category(1); ?> Range</p>
				</div>
				<div class="availability">
					<p>On Tap? or Bottled?</p>
				</div>
			</div>
			<div class="large-4 left">
				<div class="beer-sidebar">
					<h2>Awards</h2>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="large-8 left">
				<h2>Description</h2>	
			</div>
			<div class="large-4 left">
				<div class="beer-sidebar">
					<h2>Tasting Notes</h2>
				</div>
			</div>
		</div>
		<div class="row">
			<img src="#" alt="images"/>
		</div>
	</div>
</div>


<?php get_footer(); ?>
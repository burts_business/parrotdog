<?php //Functions Page
add_action( 'init', 'create_posttype' );
function create_posttype() {
	register_post_type( 'beers',
		array(
			'labels' => array(
				'name' =>  'Beers',
				'singular_name' => 'Beer',
				'add_new' => 'Add new Beer',
	            'edit_item' => 'Edit Beer',
	            'new_item' => 'New Beer',
	            'view_item' => 'View Beer',
	            'search_items' => 'Search Beers',
	            'not_found' => 'No Beers found',
	            'not_found_in_trash' => 'No Beers found in Trash'
			),
			'public' => true,
			'menu_position' => 5,
			'has_archive' => true,
			'supports' => array(
            'title',
            'editor',
            'thumbnail',
            'excerpt'
			),
			'rewrite' => array('slug' => 'beers', 'with_front' 	=> false ),
			'taxonomies' => array('category')
		)
	);
}

function add_custom_types_to_tax( $query ) {
if( is_category() || is_tag() && empty( $query->query_vars['suppress_filters'] ) ) {

// Get all your post types
$post_types = get_post_types();

$query->set( 'post_type', $post_types );
return $query;
}
}
add_filter( 'pre_get_posts', 'add_custom_types_to_tax' );


add_theme_support( 'post-thumbnails' ); 

///////////////////WOooooocommerce

remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);

add_action('woocommerce_before_main_content', 'my_theme_wrapper_start', 10);
add_action('woocommerce_after_main_content', 'my_theme_wrapper_end', 10);

function my_theme_wrapper_start() {
  echo '<section class="container">';
}

function my_theme_wrapper_end() {
  echo '</section>';
}

add_theme_support( 'woocommerce' );


?>
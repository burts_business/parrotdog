<?php get_header(); ?>
	<div class="small-12 large-12 columns" role="main">
		<div class="title-section homepage">
			<h1>INTRO TITLE SECTION</h1>
			<div class="button">Buy Now</div>
		</div>
		
		<div id="beer-section">
			<div class="row">
			
				<?php query_posts('post_type=beers&category_name=core'); ?>
						<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					<div class="large-2 beer-home left">
							<a href="<?php the_permalink(); ?>"><h3><?php the_title(); ?></h3></a>
							<?php the_content(); ?>
							<p class="button">Buy Now</p>
				</div>
					<?php endwhile; else: ?>
						<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
					<?php endif; ?>
				
			</div>
		</div>
		
		
		<div class="row clearfix">
			<div class="large-8 left">
				<p>Video</p>
			</div>
			<div class="large-4 left">
				<p>Body Copy</p>
			</div>
		</div>
		
		<div class="row clearfix">
			<div class="large-6 left">
				<img src="#" alt="brewery"/>
				<h3>The Brewery</h3>
				<p>Copy</p>
				<p class="button">Read More</p>
			</div>
			<div class="large-6 left">
				<img src="#" alt="brewery"/>
				<h3>The Brewery</h3>
				<p>Copy</p>
				<p class="button">Read More</p>
			</div>
		</div>
		
		<div class="row clearfix">
			<div class="large-8 left">
				Image
			</div>
			<div class="large-4 left">
				<h2>Featured Beer</h2>
				<p>Copy</p>
			</div>
		</div>
		
	
	<!--<?php while (have_posts()) : the_post(); ?>
		<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
			<header>
				<h1 class="entry-title"><?php the_title(); ?></h1>
			</header>
			<div class="entry-content">
				<?php the_content(); ?>
			</div>
			<footer>
				<?php wp_link_pages(array('before' => '<nav id="page-nav"><p>' . __('Pages:', 'FoundationPress'), 'after' => '</p></nav>' )); ?>
				<p><?php the_tags(); ?></p>
			</footer>
		</article>
	<?php endwhile;?>-->

	</div>
		
<?php get_footer(); ?>
<?php get_header(); ?>

<div class="small-12 large-12 columns" role="main">
	<div class="intro-section">
	<h1>INTRO SLIDESHOW</h1>
	</div>
	
	<div class="content">
		<div class="row">
			<div class="page-title large-8 clearfix">
				<h1><?php the_title(); ?></h1>
			</div>
		</div>
		<div class="row">
			<?php woocommerce_content(); ?>
		</div>
		<div class="row">
			<img src="#" alt="images"/>
		</div>
	</div>
</div>


<?php get_footer(); ?>

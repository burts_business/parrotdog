
<?php get_header(); ?>

<div class="small-12 large-12 columns" role="main">
	<div class="row">
		<div class="page-title large-8">
			<h1><?php the_title(); ?></h1>
		</div>
	</div>
	<div class="row">
		<div class="large-6 left">
			<div class="contact">
				<h2>The Office</h2>
				<p>Content</p>
			</div>
			<div class="contact">
				<h2>The Brewery</h2>
				<p>Content</p>
			</div>
		</div>
		<div class="large-6 left">
			<div class="contact">
				<h2>Trade Enquiries</h2>
				<p>Phone Number</p>
				<p>Address</p>
				<p>Contact Form</p>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>
